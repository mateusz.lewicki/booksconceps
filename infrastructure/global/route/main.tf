terraform {
  backend "s3" {
    bucket = "appsbucket.mateuszlewicki.pl"
    key = "projects/booksconcepts/global/route.tfstate"
    region = "us-east-1"
  }
}

data "ovh_domain_zone" "rootzone" {
    name = var.domain
}

# data "ovh_domain_zone" "appzone" {
#     name = var.sub_domain
# }

resource "ovh_domain_zone_record" "appzone_record" {
    zone = data.ovh_domain_zone.rootzone.name
    subdomain = var.sub_domain_name
    fieldtype = "A"
    ttl = "3600"
    target = var.target
}
