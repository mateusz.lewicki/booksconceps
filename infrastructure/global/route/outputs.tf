output "zone_id" {
  value = "${ovh_domain_zone_record.appzone_record.id}"
}

output "domain" {
  value = "${var.domain}"
}

output "sub_domain" {
  value = "${var.sub_domain}"
}
