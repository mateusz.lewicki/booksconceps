variable "domain" {
  default="mateuszlewicki.pl"
}

variable "sub_domain_name" {
  default="bookconcepts"
}
variable "sub_domain" {
  default="bookconcepts.mateuszlewicki.pl"
}

variable "target" {
  default="0.0.0.0"
}
