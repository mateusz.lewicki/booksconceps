terraform {
  backend "s3" {
    bucket = "appsbucket.mateuszlewicki.pl"
    key = "projects/booksconcepts/prod/frontend.tfstate"
    region = "us-east-1"
  }
}

data "terraform_remote_state" "route" {
  backend = "s3"
  config = {
        bucket = "appsbucket.mateuszlewicki.pl"
        key = "projects/booksconcepts/global/route.tfstate"
        region = "us-east-1"
  }
}

module "frontend" {
  source = "../../modules/certificate"
  sub_domain = data.terraform_remote_state.route.outputs.sub_domain
}
