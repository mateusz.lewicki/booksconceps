output "public_rsa" {
  value = "${tls_private_key.reg_private_key.public_key_openssh}"
}
output "private_rsa" {
  value = "${tls_private_key.reg_private_key.private_key_pem}"
}
